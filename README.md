#Cara menjalankan:
1. Jalankan database server MySQL (atau MariaDB)
2. Download repository ini (zip atau clone)
3. Jangan lupa edit file `credential.php` lalu save setelah di-edit
4. Buka command prompt, lalu masuk ke direktori tempat file `install.php` berada
5. jalankan perintah `php install.php`

Fitur:
1. Create database
2. Create tabel parent
3. Create tabel child
4. Generate CRUD (coming soon..)