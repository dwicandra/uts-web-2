 <?php
	$servername = "127.0.0.1";	// Untuk server lokal gunakan `127.0.0.1` atau `localhost`
	$username = "root";			// Username database kamu
	$password = "";				// Password untuk username di atas (kosongkan kalau tidak pakai password)
	$dbname = "myDB";			// Nama database kamu

	// Tabel parent (silahkan di-edit sendiri sesuai kebutuhan)
	// Note: tidak perlu menambahkan field `id`, 
	//       karena akan di-generate otomatis
	//       field dengan nama `id` yang 
	//       menjadi Primary Key dan 
	//       auto_increment
	$table_parent = [
		'nama_tabel' => 'xxTABLExx',
		'fields' => [
			// id (otomatis di-generate),
			[
				'nama_field' => 'xxFIELD1xx',
				'tipe_data' => 'xxTYPE1xx',
				'size' => xxSIZE1xx,
			],
			[
				'nama_field' => 'xxFIELD2xx',
				'tipe_data' => 'xxTYPE2xx',
				'size' => xxSIZE2xx,
			],
			[
				'nama_field' => 'xxFIELD3xx',
				'tipe_data' => 'xxTYPE3xx',
				'size' => xxSIZE3xx,
			],
			[
				'nama_field' => 'xxFIELD4xx',
				'tipe_data' => 'xxTYPE4xx',
				'size' => xxSIZE4xx,
			],
		]
	];
?> 