<?php
	require_once("credential.php");

	$conn = new mysqli($servername, $username, $password, $dbname);


	if ($conn->connect_error) {
	    die("!! Gagal koneksi !!" . $conn->connect_error);
	}

	$sql = "SELECT * FROM xxTABLExx";
	$result = $conn->query($sql);


	if ($result->num_rows > 0) {
	    // output data of each row
	    while($row = $result->fetch_assoc()) {
	    	$data[] = [
	    		'id' => $row['id'],
	    		'xxFIELD1xx'	=> $row['xxFIELD1xx'],
	    		'xxFIELD2xx'	=> $row['xxFIELD2xx'],
	    		'xxFIELD3xx'	=> $row['xxFIELD3xx'],
	    		'xxFIELD4xx'	=> $row['xxFIELD4xx']
	    	];
	    }
	    echo json_encode($data);
	} else {
	    echo "[]";
	}
	$conn->close();
?>