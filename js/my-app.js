var app = new Framework7({
  // App root element
  root: '#app',

  // App Name
  name: 'My App',

  // App id
  id: 'com.myapp.test',
  
  // Enable swipe panel
  panel: {
    swipe: 'left',
  },
  
  // Add default routes
  routes: [
    {
      path: '/',
      url: 'index.html',
    },
    {
      path: '/tambah-data-xxTABLExx/',
      componentUrl: 'pages/tambah_data_xxTABLExx.html',
    },
    {
      path: '/tampilkan-data-xxTABLExx/',
      componentUrl: 'pages/tampilkan_data_xxTABLExx.html',
    },
  ],
});

var mainView = app.views.create('.view-main');