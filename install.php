 <?php
 	require_once('php/credential.php');
 	/*
 	 *	STEP 1: CREATE DATABASE
 	 *  Cara menjalankan:
	 	 1. Jangan lupa edit file `credential.php` lalu save setelah di-edit
	 	 2. Buka command prompt, lalu masuk ke direktori tempat file `install.php` ini berada
	 	 3. jalankan perintah `php install.php`
 	 */

	// Create connection
	$conn = new mysqli($servername, $username, $password);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}

	// Create database
	$ok = false;
	$sql = "CREATE DATABASE $dbname";
	if ($conn->query($sql) === TRUE) {
		$ok = true;
	    echo "Database created successfully";
	} else {
		if(strpos($conn->error, "database exists")){
			$ok = true;
		} else {
	    	echo "Error creating database: " . $conn->error;
		}
	}

	$conn->close();
	// header("Location: ./install_step_2.php");
?> 

 <?php
 	/*
 	 *	STEP 2: Generate Tabel Parent
 	 */

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);

	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}

	// sql untuk membuat PARENT table
	$sql = "CREATE TABLE " . $table_parent['nama_tabel'] . "(id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,";
	foreach($table_parent['fields'] as $field){
		$sql .= sprintf("%s %s(%s) NOT NULL, ", $field['nama_field'], $field['tipe_data'],$field['size']);
	}
	$sql .= "created_at TIMESTAMP NULL, updated_at TIMESTAMP NULL)";

	echo $sql;
	if ($conn->query($sql) === TRUE) {
	    echo "Table ". $table_parent['nama_tabel'] ." created successfully";
	} else {
	    echo "Error creating table: " . $conn->error;
	}

	$conn->close();
?> 